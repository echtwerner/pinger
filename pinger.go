package main

import (
	"bytes"
	"fmt"
	"log"
	"net"
	"os/exec"
	"sync"
	"time"
	"github.com/echtwerner/actor"
	"github.com/echtwerner/cidr"
)

// Prefix
type Prefix struct {
	net     *net.IPNet
	startIP net.IP
	endIP   net.IP
	curIP   net.IP
	end     bool
}

func NewPrefix(prefix string) *Prefix {
	p := &Prefix{}
	_, p.net, _ = net.ParseCIDR(prefix)
	p.startIP, p.endIP = cidr.AddressRange(p.net)
	p.curIP = p.startIP
	p.end = false
	return p
}

// Pinger
type Pinger struct {
	prefix     *Prefix
	actor      *Actor
	NeedOutput bool
}	

func StartPinger(prefix string, actor *Actor) *Pinger {
	pinger := NewPinger(prefix, actor)
	pinger.NeedOutput = false
	pinger.Run()
	return pinger
}
func NewPinger(prefix string,actor *Actor) *Pinger {
	fmt.Println("NewPinger()")
	pinger := &Pinger{
		prefix: NewPrefix(prefix),
		actor:  actor,
	}
	return pinger
}

func (pinger *Pinger) Run() {
	fmt.Println("Pinger.Run()")
	actor := pinger.actor
	prefix := pinger.prefix
	actor.Run()
	for ip := prefix.NextIP(); !prefix.End(); ip = prefix.NextIP() {
		actor.AddIP(ip)
		if pinger.NeedOutput {
			out := actor.String()
			if out != "" {
				fmt.Printf("%q: %q\n", ip, out)
			}
		}
	}
}
	

// First and Last IP of Prefix will be never return
// Last IP will be an Empty String
func (p *Prefix) NextIP() string {
	p.curIP = cidr.Inc(p.curIP)
	r := p.curIP.String()
	if r == p.endIP.String() {
		p.end = true
		return ""
	}
	return r
}

func (p *Prefix) End() bool {
	return p.end
}

// Actor
type Actor struct {
	actionChan chan action
	lastOutput string
	wg         *sync.WaitGroup
}
type action func()

func NewActor(wg *sync.WaitGroup) *Actor {
	fmt.Println("NewActor()")
	a := &Actor{}
	a.actionChan = make(chan action, 254)
	a.lastOutput = ""
	a.wg = wg
	return a
}

// Create NewActor and Run it
func StartActor(wg *sync.WaitGroup) *Actor {
	a := NewActor(wg)
	a.Run()
	return a
}


// Run Actor -> Stop will be implemented later
func (a *Actor) Run() {
	fmt.Println("Actor.Run()")
	go a.runLoop()
}

func (a *Actor) runLoop() {
	fmt.Println("Actor.runLoop()")
	timer := time.NewTimer(2 * time.Minute)
	for {
		select {
		case action := <-a.actionChan:
			action()
			a.wg.Done()
		case <-timer.C:
			log.Println("Time out!")
		}

	}
}

func (a *Actor) AddIP(ip string) {
	fmt.Println("Actor.AddIP:", ip)
	a.wg.Add(1)
	a.actionChan <- func() {
		fmt.Println("PING:", ip)
		cmd := exec.Command("ping", ip, "-n", "1", "-w", "1")
		var out bytes.Buffer
		cmd.Stdout = &out
		cmd.Run()
		a.lastOutput = out.String()
	}
}

func (a *Actor) String() string {
	fmt.Println("Actor.String()")
	resultChan := make(chan string, 0)
	a.wg.Add(1)
	a.actionChan <- func() {
		resultChan <- a.status()
	}
	return <-resultChan
}

func (a *Actor) status() string {
	fmt.Println("Actor.status()")
	r := a.lastOutput
	a.lastOutput = ""
	return r
}

// Function

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func main() {
	start := time.Now()
	var wg sync.WaitGroup
	prefixes := []string{
		"192.168.178.0/27",
		"192.168.178.0/27",
		"192.168.178.0/27",
		"192.168.178.0/27",
		"192.168.178.0/27",
	}
	for _, prefix := range prefixes {
		a := actor.NewActor(&wg)
		pinger := NewPinger(prefix,a)
		pinger.NeedOutput = false
		pinger.Run()
	}
	wg.Wait()
	elapsed := time.Since(start)
	log.Printf("Time %s", elapsed)

}
